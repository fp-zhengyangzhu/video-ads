import { Component} from '@angular/core';
import { UserService } from './user/user.service';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user_name: string;
  user_password: string;
  login: boolean;

  constructor(private userService: UserService, private router: Router) {
    this.user_name = '';
    this.user_password = '';
    this.login = false;
  }

  async loginAction() {
    this.login = await this.userService.asyncLogin(this.user_name, this.user_password);
    if (this.login === true) {
    }
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
