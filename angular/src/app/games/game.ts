import { Rule } from './rule';

export class Game {
  app_id: number;
  name: string;
  keys: string[];
  rules: Rule[];
  constructor() {
  }
}
