import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Game } from './game';
import { GameService } from './game.service';

@Component({
  selector: 'app-game-dashboard',
  templateUrl: './game-dashboard.component.html',
  styleUrls: [ '../ads/ad-dashboard.component.css' ]
})
export class GameDashboardComponent implements OnInit {
  games: Game[] = [];
  constructor(private gameService: GameService,
  private router: Router) {}

  ngOnInit(): void {
    this.gameService.getGames()
      .then(games => this.games = games);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.gameService.create(name)
      .then(game => {
        this.games.push(game);
      });
  }

}
