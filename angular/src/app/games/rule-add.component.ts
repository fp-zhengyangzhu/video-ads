import 'rxjs/add/operator/switchMap';
import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Game } from './game';
import { GameService } from './game.service';
import { Ad } from '../ads/ad';
import { AdService } from '../ads/ad.service';
import { Rule } from './rule';

@Component({
  selector: 'app-rule-add',
  templateUrl: './rule-update.component.html',
  styleUrls: [ './rule-update.component.css' ],
})

export class RuleAddComponent implements OnInit {
  game: Game;
  country_code;
  ads: Ad[] = [];
  model = new Rule();
  platforms = [
    {name: 'ios', value: 'ios', checked: false},
    {name: 'android', value: 'android', checked: false}
  ];
  all_country = false;
  add = true;
  start_date: string;
  end_date: string;
  constructor(
    protected gameService: GameService,
    protected route: ActivatedRoute,
    protected location: Location,
    protected adService: AdService
  ) {
    this.country_code = this.gameService.country_code;
  }

  ngOnInit(): void {
    this.adService.getAds()
      .then(ads => this.ads = ads);
    this.route.params
      .switchMap((params: Params) => this.gameService.getGame(+params['app_id']))
      .subscribe(game => { this.game = game; });
    this.setForm();
  }

  onSubmit() {
    if (this.model.platform === undefined || this.model.platform.length === 0) {
      alert('please set platform!');
      return;
    }
    if (this.model.country === undefined || this.model.country.length === 0) {
      alert('please set country!');
      return;
    }
    if ('string' === typeof(this.model.product)) {
      this.model.product = this.model.product.split(',');
      for (const k in this.model.product) {
        if (this.model.product[k] === '') {
          this.model.product.splice(k, 1);
        }
      }
    }
    this.model.ad_id = +this.model.ad_id;
    this.model.start_date = Date.parse(this.start_date) / 1000;
    this.model.end_date = Date.parse(this.end_date) / 1000;
    this.updateData();
  }

  updateData() {
    this.gameService.
      createRule(this.game.app_id, this.model)
      .then(() => this.goBack());
  }

  setForm() {
    for (const key in this.country_code) {
      if (this.country_code.hasOwnProperty(key)) {
        this.country_code[key].checked = false;
      }
    }
  }

  clickPlatform() {
    this.model.platform = this.selectedPlatforms();
  }

  clickCountry() {
    this.model.country = this.selectedCountry();
  }

  clickAllCounrty() {
    if (this.all_country === true) {
      this.model.country = ['ALL'];
      for (const k in this.country_code) {
        if (this.country_code.hasOwnProperty(k)) {
          this.country_code[k].checked = false;
        }
      }
    } else {
      this.model.country = [];
    }
  }

  selectedPlatforms(): string[] {
    const ret = [];
    for (const k in this.platforms) {
      if (this.platforms[k].checked === true) {
        ret.push(this.platforms[k].value);
      }
    }
    return ret;
    // return this.platforms
    //           .filter(opt => {opt.checked;console.log(opt)})
    //           .map(opt => opt.value)
  }

  selectedCountry(): string[] {
    const ret = [];
    for (const k in this.country_code) {
      if (this.country_code[k].checked === true) {
        ret.push(this.country_code[k].code);
        this.all_country = false;
      }
    }
    return ret;
  }

  get diagnostic() { return JSON.stringify(this.model); }

  goBack(): void {
    this.location.back();
  }

}
