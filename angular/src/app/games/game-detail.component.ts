import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Game } from './game';
import { GameService } from './game.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
})
export class GameDetailComponent implements OnInit {
  game: Game;
  token: string[];
  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.gameService.getGame(+params['id']))
      .subscribe(game => {
        this.game = game;
        const len = game.keys.length;
        this.token = [];
        for (let i = 0; i < len; i++) {
          this.token[i] = (btoa(game.app_id + ':' + game.keys[i]));
        }
      });
 }

  goBack(): void {
    this.location.back();
  }



  newRule(): void {
    this.router.navigate(['/tools/new_game_rule/', this.game.app_id]);
  }

  updateRule(rule_id: number): void {
    this.router.navigate(['/tools/update_game_rule/' + this.game.app_id + '/' + rule_id]);
  }

  deleteGame() {
    this.gameService
      .delete(this.game.app_id)
      .then(() => this.goBack());
  }

  async deleteRule(rule_id: number) {
    await this.gameService.deleteRule(this.game.app_id, rule_id);
    for (const key in this.game.rules) {
      if (this.game.rules[key].rule_id === rule_id) {
        this.game.rules.splice(+key, 1);
        break;
      }
    }
  }
}
