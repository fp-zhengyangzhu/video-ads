export class Rule {
  rule_id: number;
  ad_id: number;
  ad_name: string;
  platform: string[];
  product: any;
  country: string[];
  min_app_version: string;
  max_app_version: string;
  start_date: number;
  end_date: number;
}
