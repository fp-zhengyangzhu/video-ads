import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { HttpService } from '../http.service';
import { Ad } from '../ads/ad';
import { AdService } from '../ads/ad.service';
import { Rule } from './rule';
import { Game } from './game';
import * as myGlobals from '../global';


@Injectable()
export class GameService {
  ads: Ad[] = [];

  private gameUrl = `${myGlobals.apiUrl}game_tools`;  // URL to web api

  public country_code;
  constructor(private adService: AdService, private http: HttpService) {
    this.http.get('../../assets/country_code.json')
      .subscribe(res => this.country_code = res.json());
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getGames(): Promise<Game[]> {
    return this.http.get(this.gameUrl)
               .toPromise()
               .then(this.extractGames)
               .catch(this.handleError);
  }

  private extractGames(value: Response) {
    const curGames: Game[] = [];
    const body = value.json();
    for (const k in body) {
      if (body.hasOwnProperty(k)) {
        curGames.push(body[k]);
      }
    }
    return curGames;
  }

  async getGame(id: number): Promise<Game> {
    this.ads = await this.adService.asyncGetAds();
    const url = `${this.gameUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      // .then(this.extractGame)
      .then(value => {
        const body = value.json();
        const rules: Rule[] = [];
        // console.log(this.ads);
        for (const k in body.rules) {
          if (body.rules.hasOwnProperty(k) === false) {
            continue;
          }
          for (const j in this.ads) {
              if (body.rules[k].ad_id === this.ads[j].id) {
                body.rules[k].ad_name = this.ads[j].name;
                break;
              }
          }
        rules.push(body.rules[k]);
        }
      body.rules = rules;
      return body;
      })
      .catch(this.handleError);
  }

  // private extractGame(value: Response) {
  //   let body = value.json();
  //   let rules : Rule[] = [];
  //   for (var k in body.rules) {
  //     for (var j in this.ads) {
  //         if (body.rules[k].ad_id == this.ads[j].id) {
  //           body.rules[k].ad_name = this.ads[j].name;
  //           break;
  //         }
  //     }
  //     rules.push(body.rules[k])
  //   }
  //   body.rules = rules
  //   return body
  // }

  create(name: string): Promise<Game> {
    return this.http
      .post(this.gameUrl, JSON.stringify({name: name}))
      .toPromise()
      .then(res => res.json() as Game)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.gameUrl}/${id}`;
    return this.http.delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  createRule(id: number, rule: Rule): Promise<void> {
    const url = `${this.gameUrl}/${id}/rule/`;
    // console.log(rule);
    return this.http
      .post(url, JSON.stringify(rule))
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  updateRule(id: number, rule: Rule): Promise<void> {
    const url = `${this.gameUrl}/${id}/rule/`;
    // console.log(rule);
    return this.http
      .put(url, JSON.stringify(rule))
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  async deleteRule(app_id: number, rule_id: number): Promise<void>  {
    const url = `${this.gameUrl}/${app_id}/${rule_id}`;
    return this.http
      .delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }
}
