import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import * as moment from 'moment';

import { Game } from './game';
import { GameService } from './game.service';
import { Ad } from '../ads/ad';
import { AdService } from '../ads/ad.service';
import { Rule } from './rule';
import { RuleAddComponent } from './rule-add.component';

@Component({
  selector: 'app-rule-update',
  templateUrl: './rule-update.component.html',
  styleUrls: [ './rule-update.component.css' ],
})

export class RuleUpdateComponent extends RuleAddComponent implements OnInit {
  rule_id: number;
  add = false;
  constructor(
    protected gameService: GameService,
    protected route: ActivatedRoute,
    protected location: Location,
    protected adService: AdService
  ) { super(gameService, route, location, adService); }

  ngOnInit(): void {
    this.adService.getAds()
      .then(ads => this.ads = ads);
    this.route.params.subscribe(params => { this.rule_id = +params['rule_id']; });
      this.route.params
        .switchMap((params: Params) => this.gameService.getGame(+params['app_id']))
        .subscribe(game => { this.game = game; this.setModel(); });
  }

  setModel() {
    for (const key in this.game.rules) {
      if (this.game.rules[key].rule_id === this.rule_id) {
        this.model = this.game.rules[key];
        this.setSelectedPlatform();
        this.setSelectedCountry();
        this.setSelectedDate();
        return;
      }
    }
    alert('没有找到这个广告规则...');
  }

  setSelectedPlatform() {
    for (const key in this.platforms) {
      if (this.model.platform.includes(this.platforms[key].value)) {
        this.platforms[key].checked = true;
      } else {
        this.platforms[key].checked = false;
      }
    }
  }

  setSelectedCountry() {
    if (this.model.country[0] === 'ALL') {
      this.all_country = true;
      this.clickAllCounrty();
      return;
    }
    for (const key in this.country_code) {
      if (this.model.country.includes(this.country_code[key].code)) {
        this.country_code[key].checked = true;
      } else {
        this.country_code[key].checked = false;
      }
    }
  }

  setSelectedDate() {
    this.start_date = moment(this.model.start_date * 1000).format('YYYY-MM-DD');
    this.end_date = moment(this.model.end_date * 1000).format('YYYY-MM-DD');
  }

  updateData() {
    this.gameService
      .updateRule(this.game.app_id, this.model)
      .then(() => this.goBack());
  }

}
