import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Ad } from './ad';
import { AdService } from './ad.service';

@Component({
  selector: 'app-ad-dashboard',
  templateUrl: './ad-dashboard.component.html',
  styleUrls: [ './ad-dashboard.component.css' ]
})
export class AdDashboardComponent implements OnInit {
  ads: Ad[] = [];

  constructor(private adService: AdService,
  private router: Router) { }

  ngOnInit(): void {
    this.adService.getAds()
    .then(ads => { this.ads = ads; });
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.router.navigate(['/tools/ad_add', name]);
  }
}
