import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Ad } from './ad';
import { AdService } from './ad.service';

@Component({
  selector: 'app-ad-add',
  templateUrl: './ad-detail.component.html',
})
export class AdAddComponent implements OnInit {
  ad: Ad;
  disableSave: boolean;
  add: boolean;
  constructor(
    private adService: AdService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.add = true;
    this.disableSave = true;
    this.route.params
      .switchMap((params: Params) => this.adService.createFakeAd(params['name']))
      .subscribe(ad => this.ad = ad);
  }

  save(): void {
    this.adService.create(this.ad)
      .then(() => this.goBack());
  }

  goBack(): void {
    this.location.back();
  }

  delete(): void {
    this.location.back();
  }

  onKey(): void {
    this.disableSave = false;
    const att: string[] = Object.getOwnPropertyNames(this.ad);
    for (const curAtt of att) {
      if (Object.getOwnPropertyDescriptor(this.ad, curAtt).value === '') {
        this.disableSave = true;
      }
    }
  }
}
