export class Ad {
  id: number;
  name: string;
  video_url: string;
  image_url: string;
  ios_link: string;
  android_link: string;
  constructor() {
  }
}
