import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpService } from '../http.service';

import 'rxjs/add/operator/toPromise';

import { Ad } from './ad';
import * as myGlobals from '../global';

@Injectable()
export class AdService {
  private adUrl = `${myGlobals.apiUrl}ad_tools`;  // URL to web api

  constructor(private http: HttpService) { }

  getAds(): Promise<Ad[]> {
    return this.http.get(this.adUrl)
               .toPromise()
               .then(this.extractData)
               .catch(this.handleError);
  }

  private extractData(value: Response) {
    const curAds: Ad[] = [];
    const body = value.json();
    for (const k in body) {
      if (body.hasOwnProperty(k)) {
        curAds.push(body[k]);
      }
    }
    return curAds;
  }

  async asyncGetAds(): Promise<Ad[]> {
    const response = await this.http.get(this.adUrl).toPromise();
    return this.extractData(response);
  }

  getAd(id: number): Promise<Ad> {
    const url = `${this.adUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Ad)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.adUrl}/${id}`;
    return this.http.delete(url)
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(ad: Ad): Promise<Ad> {
    return this.http
      .post(this.adUrl, JSON.stringify(ad))
      .toPromise()
      .then(res => res.json() as Ad)
      .catch(this.handleError);
  }

  update(ad: Ad): Promise<Ad> {
    return this.http
      .put(this.adUrl, JSON.stringify(ad))
      .toPromise()
      .then(() => ad)
      .catch(this.handleError);
  }

  createFakeAd(n: string): Promise<Ad> {
    const ad: Ad = {id: -1, name: n, video_url: '', image_url: '', ios_link: '', android_link: ''};
    return Promise.resolve(ad);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
