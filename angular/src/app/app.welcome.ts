import { Component, OnInit } from '@angular/core';
import { UserService } from './user/user.service';

@Component({
  selector: 'app-welcome',
  template: `
  Hello {{title}}
  `,
})
export class AppWelcomeComponent implements OnInit {
  title = 'home';
  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.title = this.userService.userName;
  }
}

