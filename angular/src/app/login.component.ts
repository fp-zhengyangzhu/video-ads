import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.html',
})
export class AppLoginComponent implements OnInit {
  user_name: string;
  user_password: string;
  login: boolean;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
  }

  async loginAction() {
    this.login = await this.userService.asyncLogin(this.user_name, this.user_password);
    if (this.login === true) {
      this.router.navigateByUrl('/tools/home');
    }
  }

}
