import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { HttpService } from '../http.service';

import 'rxjs/add/operator/toPromise';

import * as myGlobals from '../global';

@Injectable()
export class UserService {
  isLoggedIn = false;
  userName: string;
  userPassword: string;
  first = false;
  private userUrl = `${myGlobals.apiUrl}user_tools`;  // URL to web api

  constructor(private http: Http, private httpsrv: HttpService) { }

  async asyncLogin(user_name: string, user_password: string): Promise<boolean> {
    this.userName = user_name;
    this.userPassword = user_password;
    const response = await this.http.post(this.userUrl, JSON.stringify({username: user_name, password: user_password})).toPromise();
    this.isLoggedIn = response.json();
    if (this.isLoggedIn === true) {
      const base = btoa(user_name + ':' + user_password);
      this.httpsrv.tokenHeader =  base;
    }
    return this.isLoggedIn;
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
