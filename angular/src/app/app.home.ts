import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './app.home.html',
})
export class AppHomeComponent implements OnInit {
  title = 'home';

  constructor() {
  }

  ngOnInit(): void {
  }

}
