import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppHomeComponent } from './app.home';
import { PageNotFoundComponent } from './404';
import { AuthGuard } from './auth-guard.service';
import { AppLoginComponent } from './login.component';
import { AppWelcomeComponent } from './app.welcome';

import { AdDashboardComponent } from './ads/ad-dashboard.component';
import { AdDetailComponent } from './ads/ad-detail.component';
import { AdAddComponent } from './ads/ad-add.component';

import { GameDashboardComponent } from './games/game-dashboard.component';
import { GameDetailComponent } from './games/game-detail.component';
import { RuleUpdateComponent } from './games/rule-update.component';
import { RuleAddComponent } from './games/rule-add.component';

import { DocComponent } from './docs/doc.component';


const routes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: AppLoginComponent },
  {
    path: 'tools', canActivate: [AuthGuard], component: AppHomeComponent,
    children:
    [
      { path: 'home', canActivate: [AuthGuard], component: AppWelcomeComponent },
      { path: 'Ads', canActivate: [AuthGuard], component: AdDashboardComponent },
      { path: 'ad_detail/:id', canActivate: [AuthGuard], component: AdDetailComponent },
      { path: 'ad_add/:name', canActivate: [AuthGuard], component: AdAddComponent },
      { path: 'Games', canActivate: [AuthGuard], component: GameDashboardComponent },
      { path: 'game_detail/:id', canActivate: [AuthGuard], component: GameDetailComponent },
      { path: 'update_game_rule/:app_id/:rule_id', canActivate: [AuthGuard], component: RuleUpdateComponent },
      { path: 'new_game_rule/:app_id', canActivate: [AuthGuard], component: RuleAddComponent },
      { path: 'doc', canActivate: [AuthGuard], component: DocComponent },
    ]
  },
  { path: '**', canActivate: [AuthGuard], component: PageNotFoundComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
