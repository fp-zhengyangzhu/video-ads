import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  selector: 'app-doc',
  templateUrl: './doc.component.html'
})
export class DocComponent implements OnInit {
  country_code;
  country_wrap: number[];
  country_slice: number[];
  platforms = ['ios', 'android'];

  constructor(private http: Http) { }

  async ngOnInit() {
    const response = await this.http.get('../../assets/country_code.json').toPromise();
    this.country_code = response.json();
    const length = Object.keys(this.country_code).length;
    const col = Math.floor(length / 3);
    const col2 = col + col;
    this.country_wrap = [0, col, col2];
    this.country_slice = [col, col2, length];
  }

}
