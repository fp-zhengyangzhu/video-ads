import { Component } from '@angular/core';

@Component({
  selector: 'app-side-bar',
  template: `
  <ul class="list-group">
   	<li class="list-group-item" *ngFor="let name of names"><a routerLink="/tools/{{name}}" routerLinkActive="active">{{ name }}</a></li>
  </ul>
  `,
})
export class AppSidebarComponent {
  names: string[];
  constructor() {
    this.names = ['Games', 'Ads', 'Reports'];
  }
}
