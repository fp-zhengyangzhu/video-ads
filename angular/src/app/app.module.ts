import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, XHRBackend, RequestOptions } from '@angular/http';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// layout
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './404';
import { AppComponent } from './app.component';
import { AppHomeComponent } from './app.home';
import { AppSidebarComponent } from './app.sidebar';
import { requestOptionsProvider, DefaultRequestOptions } from './default-request-options.service';
import { AuthGuard } from './auth-guard.service';
import { HttpService } from './http.service';
import { AppLoginComponent } from './login.component';
import { AppWelcomeComponent } from './app.welcome';

// ad
import { AdDashboardComponent } from './ads/ad-dashboard.component';
import { AdDetailComponent } from './ads/ad-detail.component';
import { AdAddComponent } from './ads/ad-add.component';
import { AdService } from './ads/ad.service';

// game
import { GameService } from './games/game.service';
import { GameDashboardComponent } from './games/game-dashboard.component';
import { GameDetailComponent } from './games/game-detail.component';
import { RuleUpdateComponent } from './games/rule-update.component';
import { RuleAddComponent } from './games/rule-add.component';

// docs
import { DocComponent } from './docs/doc.component';

// user
import { UserService } from './user/user.service';

export function httpFactory(backend: XHRBackend, defaultOptions: RequestOptions) {
  return  new HttpService(backend, defaultOptions);
}

@NgModule({
  declarations: [
    AppComponent,
    AppHomeComponent,
    AppSidebarComponent,
    PageNotFoundComponent,
    AdDashboardComponent,
    AdDetailComponent,
    AdAddComponent,
    GameDashboardComponent,
    GameDetailComponent,
    RuleUpdateComponent,
    RuleAddComponent,
    DocComponent,
    AppLoginComponent,
    AppWelcomeComponent,
  ],
  imports: [
//  NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [AdService, GameService, requestOptionsProvider, UserService, AuthGuard, DefaultRequestOptions,
  { provide: HttpService,
    deps: [XHRBackend, RequestOptions],
    useFactory: httpFactory
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
