package aws

import (
	"fmt"
	"os"

	myconf "bitbucket.org/funplus/familyfarm-server-midware/conf"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Options struct {
	Bucket                string `cfg:"bucket" default:"0"`
	Aws_access_key_id     string `cfg:"aws_access_key_id" default:"0"`
	Aws_secret_access_key string `cfg:"aws_secret_access_key" default:"0"`
	Regin                 string `cfg:"regin" default:"0"`
}

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func initS3() (*s3.S3, string) {
	opts := &Options{}
	myconf.LoadFile(opts, "config.toml")
	token := ""
	creds := credentials.NewStaticCredentials(opts.Aws_access_key_id, opts.Aws_secret_access_key, token)
	_, err := creds.Get()

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	sess := session.Must(session.NewSession(&aws.Config{
		Region:      aws.String(opts.Regin),
		Credentials: creds,
	}))
	return s3.New(sess), opts.Bucket
}
