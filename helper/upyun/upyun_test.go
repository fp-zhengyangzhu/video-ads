package upyun

import (
	"fmt"
	"github.com/upyun/go-sdk/upyun"
	"testing"
)

func TestMain(m *testing.M) {
	m.Run()
}

func Test_Upyun(t *testing.T) {
	up := initUpyun()
	// 上传文件
	// fmt.Println(up.Put(&upyun.PutObjectConfig{
	// 	Path:      "/demo.log",
	// 	LocalPath: "/tmp/upload",
	// }))

	// // 下载
	// fmt.Println(up.Get(&upyun.GetObjectConfig{
	// 	Path:      "/demo.log",
	// 	LocalPath: "/tmp/download",
	// }))
	res, _ := up.GetInfo("conf/server_url_chinamobile.conf")
	fmt.Println(res)
	// 列目录
	objsChan := make(chan *upyun.FileInfo, 10)
	go func() {
		fmt.Println(up.List(&upyun.GetObjectsConfig{
			Path:           "/apk",
			ObjectsChan:    objsChan,
			MaxListObjects: 10,
			MaxListTries:   1,
			MaxListLevel:   2,
		}))
	}()
	for obj := range objsChan {
		fmt.Println(obj)
	}
}
