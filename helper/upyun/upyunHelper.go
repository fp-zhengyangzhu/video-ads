package upyun

import (
	myconf "bitbucket.org/funplus/familyfarm-server-midware/conf"
	"github.com/upyun/go-sdk/upyun"
)

type Options struct {
	Bucket   string `cfg:"bucket" default:"0"`
	Operator string `cfg:"operator" default:"0"`
	Password string `cfg:"password" default:"0"`
}

func initUpyun() *upyun.UpYun {
	opts := &Options{}
	myconf.LoadFile(opts, "config.toml")
	return upyun.NewUpYun(&upyun.UpYunConfig{
		Bucket:   opts.Bucket,
		Operator: opts.Operator,
		Password: opts.Password,
	})
}
