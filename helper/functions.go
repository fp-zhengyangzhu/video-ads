package helper

import (
	"encoding/binary"
	//"fmt"
	"math/rand"
	"reflect"
	"strconv"
	"strings"
)

func Itob(v int) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

func RandString(n int) string {
	letterBytes := "abcdefghjkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789"
	var letterIdxBits uint = 6                     // 6 bits to represent a letter index
	var letterIdxMask int64 = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax := 63 / letterIdxBits             // # of letter indices fitting in 63 bits
	b := make([]byte, n)
	// A rand.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, rand.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

// min <= target <= max 时返回 true
func VersionCompare(target, min, max string) bool {
	targetArr := strings.Split(target, ".")
	minArr := strings.Split(min, ".")
	maxArr := strings.Split(max, ".")
	targetLen := len(targetArr)
	minLen := len(minArr)
	maxLen := len(maxArr)
	compareMin, compareMax := true, true

	// 补充target的长度
	a := minLen - targetLen
	b := maxLen - targetLen
	for a > 0 || b > 0 {
		targetArr = append(targetArr, "0")
		a--
		b--
	}
	targetLen = len(targetArr)

	for i := 0; i < targetLen; i++ {
		targetV, _ := strconv.Atoi(targetArr[i])
		minV, maxV := 0, 0
		// 比较min version
		if compareMin {
			if minLen > i { //存在小版本号
				minV, _ = strconv.Atoi(minArr[i])
			}
			if minV > targetV {
				return false
			} else if minV < targetV {
				compareMin = false
			} else { // 相同则继续比较
				compareMin = true
			}
		}
		if compareMax {
			if maxLen > i {
				maxV, _ = strconv.Atoi(maxArr[i])
			}
			if maxV < targetV {
				return false
			} else if maxV > targetV {
				compareMax = false
			} else {
				compareMax = true
			}
		}
	}
	return true
}

func In_array(val interface{}, array interface{}) (exists bool) {
	exists = false
	//index = -1

	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)

		for i := 0; i < s.Len(); i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				//index = i
				exists = true
				return
			}
		}
	}

	return
}
