package models

import (
	"encoding/json"
	"errors"
	//"fmt"
	"sync"
	"time"

	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/service/boltfile"
	"bitbucket.org/fp-zhengyang.zhu/video-ads/helper"
	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

const (
	game_bucket_name = "video_ad_game"
)

type GameModel struct {
	games map[int]*GameItem
	lock  *sync.RWMutex
	db    *bolt.DB
}

type GameItem struct {
	Id    int          `json:"app_id"`
	Name  string       `json:"name"`
	Keys  []string     `json:"keys"`
	Rules map[int]Rule `json:"rules"`
}

type Rule struct {
	Rule_id         int      `json:"rule_id"`
	Ad_id           int      `json:"ad_id"`
	Platform        []string `json:"platform"`
	Product         []string `json:"product"`
	Country         []string `json:"country"`
	Min_app_version string   `json:"min_app_version"`
	Max_app_version string   `json:"max_app_version"`
	Start_date      int64    `json:"start_date"`
	End_date        int64    `json:"end_date"`
}

type ClientInfo struct {
	Platform    string `json:"platform"`
	Product     string `json:"product"`
	Country     string `json:"country"`
	App_version string `json:"app_version"`
}

var game *GameModel = nil
var game_once sync.Once

func GetGameModel() *GameModel {
	game_once.Do(func() {
		game = new(GameModel)
		game.games = make(map[int]*GameItem)
		game.lock = new(sync.RWMutex)
		game.db = boltfile.GetBoltDB()
		game.db.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucketIfNotExists([]byte(game_bucket_name))
			if err != nil {
				panic("game bolt update failed")
			}
			return nil
		})
		game.load()
		beego.Debug("GameModel init done")
	})
	return game
}

func (this *GameModel) load() error {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(game_bucket_name))
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			item := new(GameItem)
			err := json.Unmarshal(v, item)
			if err != nil {
				beego.Error("load error")
			}
			//beego.Debug(k)
			this.putIntoMap(item)
		}
		return nil
	})
	return nil
}

func (this *GameModel) putIntoMap(item *GameItem) {
	app_id := item.Id
	this.games[app_id] = item
}

func (this *GameModel) Push(item *GameItem) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(game_bucket_name))
		id64, _ := b.NextSequence()
		id := int(id64)
		item.Id = id
		item.Rules = make(map[int]Rule)
		item.Keys = []string{helper.RandString(16)}
		buf, err := json.Marshal(item)
		if err != nil {
			return err
		}
		this.putIntoMap(item)
		return b.Put(helper.Itob(id), buf)
	})
	return nil
}

func (this *GameModel) GetAll() map[int]*GameItem {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.games
}

func (this *GameModel) GetOne(app_id int) (GameItem, error) {
	this.lock.RLock()
	defer this.lock.RUnlock()
	if v, ok := this.games[app_id]; ok {
		return *v, nil
	}
	return GameItem{}, errors.New("app_id Not Exist")
}

func (this *GameModel) DeleteOne(app_id int) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	if _, ok := this.games[app_id]; !ok {
		return errors.New("app_id Not Exist")
	}
	err := this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(game_bucket_name))
		//beego.Debug(helper.Itob(app_id))
		err := b.Delete(helper.Itob(app_id))
		return err
	})
	if err != nil {
		beego.Error("delete game failed")
	}
	delete(this.games, app_id)
	return nil
}

func (this *GameModel) UpdateOne(item GameItem) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	return nil
}

func (this *GameModel) CreateRule(app_id int, rule Rule) (*GameItem, error) {
	this.lock.Lock()
	defer this.lock.Unlock()
	if _, ok := this.games[app_id]; !ok {
		return nil, errors.New("app_id Not Exist")
	}
	game := this.games[app_id]
	maxRuleId := 0
	for index, _ := range game.Rules {
		if index > maxRuleId {
			maxRuleId = index
		}
	}
	rule.Rule_id = maxRuleId + 1
	game.Rules[rule.Rule_id] = rule
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(game_bucket_name))
		buf, err := json.Marshal(game)
		if err != nil {
			return err
		}
		return b.Put(helper.Itob(app_id), buf)
	})
	return game, nil
}

func (this *GameModel) DeleteOneRule(app_id, rule_id int) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	if _, ok := this.games[app_id]; !ok {
		return errors.New("app_id Not Exist")
	}
	if _, ok := this.games[app_id].Rules[rule_id]; !ok {
		return errors.New("rule_id Not Exist")
	}
	game := this.games[app_id]
	delete(game.Rules, rule_id)
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(game_bucket_name))
		buf, err := json.Marshal(game)
		if err != nil {
			return err
		}
		return b.Put(helper.Itob(app_id), buf)
	})
	return nil
}

func (this *GameModel) UpdateRule(app_id int, rule Rule) (*GameItem, error) {
	this.lock.Lock()
	defer this.lock.Unlock()
	rule_id := rule.Rule_id
	if _, ok := this.games[app_id]; !ok {
		return nil, errors.New("app_id Not Exist")
	}
	if _, ok := this.games[app_id].Rules[rule_id]; !ok {
		return nil, errors.New("rule_id Not Exist")
	}
	game := this.games[app_id]
	game.Rules[rule_id] = rule
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(game_bucket_name))
		buf, err := json.Marshal(game)
		if err != nil {
			return err
		}
		return b.Put(helper.Itob(app_id), buf)
	})
	return game, nil
}

func (this *GameModel) RecommendAd(app_id int, cf ClientInfo) (int, error) {
	this.lock.RLock()
	defer this.lock.RUnlock()
	if _, ok := this.games[app_id]; !ok {
		return -1, errors.New("app_id Not Exist")
	}
	for _, rule := range this.games[app_id].Rules {
		// check time limit
		now := time.Now().Unix()
		if now < rule.Start_date || now > rule.End_date {
			continue
		}
		// check platform
		if helper.In_array(cf.Platform, rule.Platform) == false {
			continue
		}
		//check product
		if helper.In_array(cf.Product, rule.Product) == false {
			continue
		}
		//check app version
		if helper.VersionCompare(cf.App_version, rule.Min_app_version, rule.Max_app_version) == false {
			continue
		}
		//check country
		exists := false
		for _, country := range rule.Country {
			if country == "ALL" || country == cf.Country {
				exists = true
				break
			}
		}
		if exists == false {
			continue
		}
		//pass check
		return rule.Ad_id, nil
	}
	return -1, errors.New("No suitable ad")
}

func (this *GameModel) CheckKey(app_id int, key string) bool {
	this.lock.RLock()
	defer this.lock.RUnlock()
	if _, ok := this.games[app_id]; !ok {
		return false
	}
	if helper.In_array(key, this.games[app_id].Keys) == false {
		return false
	}
	return true
}
