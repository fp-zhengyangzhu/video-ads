package models

import (
	"encoding/json"
	"errors"
	"sync"

	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/service/boltfile"
	"bitbucket.org/fp-zhengyang.zhu/video-ads/helper"
	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
)

const (
	ad_bucket_name = "video_ad_ad"
)

type AdModel struct {
	ads  map[int]*AdItem
	lock *sync.RWMutex
	db   *bolt.DB
}

type AdItem struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	VideoUrl    string `json:"video_url"`
	ImageUrl    string `json:"image_url"`
	IOSLink     string `json:"ios_link"`
	AndroidLink string `json:"android_link"`
}

var ad *AdModel = nil
var once sync.Once

func GetAdModel() *AdModel {
	once.Do(func() {
		ad = new(AdModel)
		ad.ads = make(map[int]*AdItem)
		ad.lock = new(sync.RWMutex)
		ad.db = boltfile.GetBoltDB()
		ad.db.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucketIfNotExists([]byte(ad_bucket_name))
			if err != nil {
				panic("ad bolt update failed")
			}
			return nil
		})
		ad.load()
		beego.Debug("AdModel init done")
	})
	return ad
}

func (this *AdModel) load() error {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(ad_bucket_name))
		err := b.ForEach(func(k, v []byte) error {
			item := new(AdItem)
			err := json.Unmarshal(v, item)
			if err != nil {
				return err
			}
			this.putIntoMap(item)
			return nil
		})
		if err != nil {
			return err
		}
		return nil
	})
	return nil
}

func (this *AdModel) putIntoMap(item *AdItem) {
	ad_id := item.Id
	this.ads[ad_id] = item
}

func (this *AdModel) Push(item *AdItem) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(ad_bucket_name))
		id64, _ := b.NextSequence()
		id := int(id64)
		item.Id = id
		buf, err := json.Marshal(item)
		if err != nil {
			return err
		}
		this.putIntoMap(item)
		return b.Put(helper.Itob(id), buf)
	})
	return nil
}

// func (this *AdModel) GetAll() []AdItem {
// 	this.lock.RLock()
// 	defer this.lock.RUnlock()
// 	var res []AdItem
// 	for _, item := range this.ads {
// 		res = append(res, *item)
// 	}
// 	return res
// }

func (this *AdModel) GetAll() map[int]*AdItem {
	this.lock.RLock()
	defer this.lock.RUnlock()
	return this.ads
}

func (this *AdModel) GetOne(ad_id int) (*AdItem, error) {
	this.lock.RLock()
	defer this.lock.RUnlock()
	if v, ok := this.ads[ad_id]; ok {
		return v, nil
	}
	return nil, errors.New("ad_id Not Exist")
}

func (this *AdModel) DeleteOne(ad_id int) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	if _, ok := this.ads[ad_id]; !ok {
		return errors.New("ad_id Not Exist")
	}
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(ad_bucket_name))
		err := b.Delete(helper.Itob(ad_id))
		return err
	})
	delete(this.ads, ad_id)
	return nil
}

func (this *AdModel) UpdateOne(item AdItem) error {
	this.lock.Lock()
	defer this.lock.Unlock()
	ad_id := item.Id
	if _, ok := this.ads[ad_id]; !ok {
		return errors.New("ad_id Not Exist")
	}
	this.ads[ad_id].AndroidLink = item.AndroidLink
	this.ads[ad_id].IOSLink = item.IOSLink
	this.ads[ad_id].ImageUrl = item.ImageUrl
	this.ads[ad_id].Name = item.Name
	this.ads[ad_id].VideoUrl = item.VideoUrl
	this.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(ad_bucket_name))
		buf, err := json.Marshal(item)
		if err != nil {
			return err
		}
		return b.Put(helper.Itob(ad_id), buf)
	})

	return nil
}
