package controllers

import (
	"encoding/json"
	//"fmt"
	"strconv"

	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/models"
	//"github.com/astaxie/beego"
)

type GameToolsController struct {
	ToolsBaseController
}

// @Title GetAll
// @Description get all games
// @Success 200 {object} models.GameItem
// @Failure 500 internal error
// @router / [get]
func (this *GameToolsController) GetAll() {
	allGames := models.GetGameModel().GetAll()
	this.Data["json"] = allGames
	this.ServeJSON()
}

// @Title Get
// @Description find game by app_id
// @Param	app_id		path 	int	true		"the app_id you want to get"
// @Success 200 {object} models.GameItem
// @Failure 403 :app_id is empty
// @router /:app_id [get]
func (this *GameToolsController) Get() {
	para := this.Ctx.Input.Param(":app_id")
	if app_id, error := strconv.Atoi(para); error == nil {
		game, err := models.GetGameModel().GetOne(app_id)
		if err != nil {
			this.Data["json"] = err.Error()
		} else {
			this.Data["json"] = game
		}
	}
	this.ServeJSON()
}

// @Title Create
// @Description create game
// @Param	body		body 	models.GameItem	true		"The game name"
// @Success 200 {object} models.GameItem
// @Failure 403 body is empty
// @router / [post]
func (this *GameToolsController) Post() {
	var GameItem models.GameItem
	json.Unmarshal(this.Ctx.Input.RequestBody, &GameItem)
	models.GetGameModel().Push(&GameItem)
	this.Data["json"] = GameItem
	this.ServeJSON()
}

// @Title Update
// @Description update the game
// @Param	body		body 	models.GameItem	true		"The body"
// @Success 200 {object} models.GameItem
// @Failure 403 body is empty
// @router / [put]
func (this *GameToolsController) Put() {
	var GameItem models.GameItem
	json.Unmarshal(this.Ctx.Input.RequestBody, &GameItem)
	err := models.GetGameModel().UpdateOne(GameItem)
	if err != nil {
		this.Data["json"] = err.Error()
	} else {
		this.Data["json"] = GameItem
	}
	this.ServeJSON()
}

// @Title Delete
// @Description delete the game
// @Param	app_id		path 	int	true		"The app_id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 app_id is empty
// @router /:app_id [delete]
func (this *GameToolsController) Delete() {
	para := this.Ctx.Input.Param(":app_id")
	if app_id, error := strconv.Atoi(para); error == nil {
		err := models.GetGameModel().DeleteOne(app_id)
		if err != nil {
			this.Data["json"] = err.Error()
		} else {
			this.Data["json"] = "delete success!"
		}
	}
	this.ServeJSON()
}

// @Title CreateRule
// @Description create rule
// @Param	body		body 	models.Rule	true "The rule content"
// @Success 200 {object} models.GameItem
// @Failure 403 body is empty
// @router /:app_id/rule [post]
func (this *GameToolsController) PostRule() {
	para := this.Ctx.Input.Param(":app_id")
	app_id, paraError := strconv.Atoi(para)
	if paraError != nil {
		this.Data["json"] = "app_id not correct"
		this.ServeJSON()
		return
	}
	var RuleItem models.Rule
	json.Unmarshal(this.Ctx.Input.RequestBody, &RuleItem)
	GameItem, error := models.GetGameModel().CreateRule(app_id, RuleItem)
	if error != nil {
		this.Data["json"] = error.Error()
	} else {
		this.Data["json"] = GameItem
	}
	this.ServeJSON()
}

// @Title DeleteRule
// @Description delete the rule
// @Param	app_id		path 	int	true		"The app_id you want to delete"
// @Param	rule_id		path 	int	true		"The rule_id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 app_id or rule_id is empty
// @router /:app_id/:rule_id [delete]
func (this *GameToolsController) DeleteRule() {
	para1 := this.Ctx.Input.Param(":app_id")
	para2 := this.Ctx.Input.Param(":rule_id")
	app_id, paraError1 := strconv.Atoi(para1)
	if paraError1 != nil {
		this.Data["json"] = "app_id not correct"
		this.ServeJSON()
		return
	}
	rule_id, paraError2 := strconv.Atoi(para2)
	if paraError2 != nil {
		this.Data["json"] = "rule_id not correct"
		this.ServeJSON()
		return
	}
	err := models.GetGameModel().DeleteOneRule(app_id, rule_id)
	if err != nil {
		this.Data["json"] = err.Error()
	} else {
		this.Data["json"] = "delete success!"
	}
	this.ServeJSON()
}

// @Title UpdateRule
// @Description update rule
// @Param	body		body 	models.Rule	true "The rule content"
// @Success 200 {object} models.GameItem
// @Failure 403 body is empty
// @router /:app_id/rule [put]
func (this *GameToolsController) UpdateRule() {
	para := this.Ctx.Input.Param(":app_id")
	app_id, paraError := strconv.Atoi(para)
	if paraError != nil {
		this.Data["json"] = "app_id not correct"
		this.ServeJSON()
		return
	}
	var RuleItem models.Rule
	json.Unmarshal(this.Ctx.Input.RequestBody, &RuleItem)
	GameItem, error := models.GetGameModel().UpdateRule(app_id, RuleItem)
	if error != nil {
		this.Data["json"] = error.Error()
	} else {
		this.Data["json"] = GameItem
	}
	this.ServeJSON()
}

// @Title Options
// @Description options test
// @Success 200 {string} options success!
// @Failure 500 internal error
// @router /* [options]
func (this *GameToolsController) Options() {
	this.Data["json"] = "options success!"
	this.ServeJSON()
}
