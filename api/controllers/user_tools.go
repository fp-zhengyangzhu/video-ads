package controllers

import (
	"encoding/json"
	//"fmt"
	"github.com/astaxie/beego"
)

type UserToolsController struct {
	beego.Controller
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// @Title Post
// @Description check user
// @Param	body		body 	User	true		"The user name and password"
// @Success 200 {bool} success
// @Failure 403 body is empty
// @router / [post]
func (this *UserToolsController) Post() {
	var user User
	json.Unmarshal(this.Ctx.Input.RequestBody, &user)
	realPwd := beego.AppConfig.DefaultString("users::"+user.Username, "")
	if user.Username == "" || user.Password == "" || realPwd == "" || user.Password != realPwd {
		this.Data["json"] = false
	} else {
		this.Data["json"] = true
	}
	this.ServeJSON()
}

// @Title Options
// @Description options test
// @Success 200 {string} options success!
// @Failure 500 internal error
// @router /* [options]
func (this *UserToolsController) Options() {
	this.Data["json"] = "options success!"
	this.ServeJSON()
}

func (this *UserToolsController) Prepare() {
	this.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
	this.Ctx.Output.Header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS")
	this.Ctx.Output.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Token")
}
