package controllers

import (
	"encoding/base64"
	//"fmt"
	"github.com/astaxie/beego"
	"strings"
)

type ToolsBaseController struct {
	beego.Controller
}

func (this *ToolsBaseController) Prepare() {

	if this.Ctx.Input.Method() != "OPTIONS" {
		token := this.Ctx.Input.Header("Token")
		if token == "" {
			this.Abort("401")
		}
		decoded, err := base64.StdEncoding.DecodeString(token)
		if err != nil {
			this.Abort("401")
		}
		decodedStringArr := strings.Split(string(decoded), ":")
		userName := decodedStringArr[0]
		password := decodedStringArr[1]
		if beego.AppConfig.DefaultString("users::"+userName, "") != password {
			this.Abort("401")
		}
	}

	this.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
	this.Ctx.Output.Header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS")
	this.Ctx.Output.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Token")
}

func (this *ToolsBaseController) Finish() {

}
