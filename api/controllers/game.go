package controllers

import (
	"encoding/json"

	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/models"
	//"github.com/astaxie/beego"
)

type GameController struct {
	BaseController
}

type RetrunData struct {
	Data interface{} `json:"data"`
	Err  string      `json:"error"`
}

// @Title Post
// @Description fetch ad by app_id and clinet info
// @Param	body		body 	models.ClientInfo	true		"The body"
// @Success 200 {object} models.AdItem
// @router / [post]
func (this *GameController) Post() {
	var cf models.ClientInfo
	json.Unmarshal(this.Ctx.Input.RequestBody, &cf)
	ad_id, gameErr := models.GetGameModel().RecommendAd(this.app_id, cf)
	var ret = RetrunData{nil, ""}
	if gameErr != nil {
		ret.Err = gameErr.Error()
		this.Data["json"] = ret
		this.ServeJSON()
		this.StopRun()
	}
	ad, adErr := models.GetAdModel().GetOne(ad_id)
	if adErr != nil {
		ret.Err = adErr.Error()
	} else {
		ret.Data = ad
	}
	this.Data["json"] = ret
	this.ServeJSON()
}
