package controllers

import (
	"github.com/astaxie/beego"
)

type ErrorController struct {
	beego.Controller
}

func (c *ErrorController) Error404() {
	c.Ctx.ResponseWriter.WriteHeader(404)
	c.Ctx.WriteString("Not Found")
}

func (c *ErrorController) Error401() {
	c.Ctx.ResponseWriter.WriteHeader(401)
	c.Ctx.WriteString("Unauthorized")
}
