package controllers

import (
	"encoding/json"
	//"fmt"
	"strconv"

	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/models"
	//"github.com/astaxie/beego"
)

type AdToolsController struct {
	ToolsBaseController
}

// @Title GetAll
// @Description get all ads
// @Success 200 {object} models.AdItem
// @Failure 500 internal error
// @router / [get]
func (this *AdToolsController) GetAll() {
	allAds := models.GetAdModel().GetAll()
	this.Data["json"] = allAds
	this.ServeJSON()
}

// @Title Get
// @Description find ad by ad_id
// @Param	ad_id		path 	int	true		"the ad_id you want to get"
// @Success 200 {object} models.AdItem
// @Failure 403 :ad_id is empty
// @router /:ad_id [get]
func (this *AdToolsController) Get() {
	para := this.Ctx.Input.Param(":ad_id")
	if ad_id, error := strconv.Atoi(para); error == nil {
		ad, err := models.GetAdModel().GetOne(ad_id)
		if err != nil {
			this.Data["json"] = err.Error()
		} else {
			this.Data["json"] = ad
		}
	}
	this.ServeJSON()
}

// @Title Create
// @Description create ad
// @Param	body		body 	models.AdItem	true		"The object content"
// @Success 200 {object} models.AdItem
// @Failure 403 body is empty
// @router / [post]
func (this *AdToolsController) Post() {
	var AdItem models.AdItem
	json.Unmarshal(this.Ctx.Input.RequestBody, &AdItem)
	models.GetAdModel().Push(&AdItem)
	this.Data["json"] = AdItem
	this.ServeJSON()
}

// @Title Update
// @Description update the ad
// @Param	body		body 	models.AdItem	true		"The body"
// @Success 200 {object} models.AdItem
// @Failure 403 body is empty
// @router / [put]
func (this *AdToolsController) Put() {
	var AdItem models.AdItem
	json.Unmarshal(this.Ctx.Input.RequestBody, &AdItem)
	err := models.GetAdModel().UpdateOne(AdItem)
	if err != nil {
		this.Data["json"] = err.Error()
	} else {
		this.Data["json"] = AdItem
	}
	this.ServeJSON()
}

// @Title Delete
// @Description delete the ad
// @Param	ad_id		path 	int	true		"The ad_id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 ad_id is empty
// @router /:ad_id [delete]
func (this *AdToolsController) Delete() {
	para := this.Ctx.Input.Param(":ad_id")
	if ad_id, error := strconv.Atoi(para); error == nil {
		err := models.GetAdModel().DeleteOne(ad_id)
		if err != nil {
			this.Data["json"] = err.Error()
		} else {
			this.Data["json"] = "delete success!"
		}
	}
	this.ServeJSON()
}

// @Title Options
// @Description options test
// @Success 200 {string} options success!
// @Failure 500 internal error
// @router /* [options]
func (this *AdToolsController) Options() {
	this.Data["json"] = "options success!"
	this.ServeJSON()
}
