package controllers

import (
	"encoding/base64"
	//"fmt"
	"github.com/astaxie/beego"
	"strconv"
	"strings"

	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/models"
)

type BaseController struct {
	beego.Controller
	app_id int
}

func (this *BaseController) Prepare() {
	token := this.Ctx.Input.Header("Token")
	if token == "" {
		this.Abort("401")
	}
	decoded, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		this.Abort("401")
	}
	decodedStringArr := strings.Split(string(decoded), ":")
	app_id, _ := strconv.Atoi(decodedStringArr[0])
	key := decodedStringArr[1]
	if models.GetGameModel().CheckKey(app_id, key) == false {
		this.Abort("401")
	}
	this.app_id = app_id
}

func (this *BaseController) Finish() {
}
