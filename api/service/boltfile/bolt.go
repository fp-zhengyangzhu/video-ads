package boltfile

import (
	"github.com/astaxie/beego"
	"github.com/boltdb/bolt"
	"time"
)

type Bolt struct {
	DB *bolt.DB
}

var BoltSrv *Bolt = nil

func New() {
	BoltSrv = new(Bolt)
	db_file := beego.AppConfig.String("boltLocation")
	db, err := bolt.Open(db_file, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		panic("bolt open failed")
	}
	BoltSrv.DB = db
}

func GetBoltDB() *bolt.DB {
	if BoltSrv == nil || BoltSrv.DB == nil {
		panic("bolt open failed")
	}
	return BoltSrv.DB
}

func Close() error {
	if BoltSrv != nil && BoltSrv.DB != nil {
		BoltSrv.DB.Close()
	}
	return nil
}
