package main

import (
	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers"
	_ "bitbucket.org/fp-zhengyang.zhu/video-ads/api/routers"
	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/service/boltfile"
	"github.com/astaxie/beego"
)

var logLevel = map[string]int{
	"emergency": beego.LevelEmergency,
	"alert":     beego.LevelAlert,
	"critical":  beego.LevelCritical,
	"error":     beego.LevelError,
	"warning":   beego.LevelWarning,
	"notice":    beego.LevelNotice,
	"info":      beego.LevelInformational,
	"debug":     beego.LevelDebug,
}

func main() {
	boltfile.New()
	defer boltfile.Close()
	beego.SetLevel(logLevel[beego.AppConfig.String("loglevel")])
	beego.SetLogFuncCall(true)
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	// beego.Emergency("this is emergency")
	// beego.Alert("this is alert")
	// beego.Critical("this is critical")
	// beego.Error("this is error")
	// beego.Warning("this is warning")
	// beego.Notice("this is notice")
	// beego.Informational("this is informational")
	// beego.Debug("this is debug")
	beego.ErrorController(&controllers.ErrorController{})
	beego.Run()
}
