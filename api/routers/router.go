// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers"
	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/v1",
		beego.NSNamespace("/game",
			beego.NSInclude(
				&controllers.GameController{},
			),
		),
		beego.NSNamespace("/ad_tools",
			beego.NSInclude(
				&controllers.AdToolsController{},
			),
		),
		beego.NSNamespace("/game_tools",
			beego.NSInclude(
				&controllers.GameToolsController{},
			),
		),
		beego.NSNamespace("/user_tools",
			beego.NSInclude(
				&controllers.UserToolsController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
