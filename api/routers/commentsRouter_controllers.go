package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:ad_id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:ad_id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:AdToolsController"],
		beego.ControllerComments{
			Method: "Options",
			Router: `/*`,
			AllowHTTPMethods: []string{"options"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "GetAll",
			Router: `/`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "Get",
			Router: `/:app_id`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "Put",
			Router: `/`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "Delete",
			Router: `/:app_id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "PostRule",
			Router: `/:app_id/rule`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "DeleteRule",
			Router: `/:app_id/:rule_id`,
			AllowHTTPMethods: []string{"delete"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "UpdateRule",
			Router: `/:app_id/rule`,
			AllowHTTPMethods: []string{"put"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:GameToolsController"],
		beego.ControllerComments{
			Method: "Options",
			Router: `/*`,
			AllowHTTPMethods: []string{"options"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:UserToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:UserToolsController"],
		beego.ControllerComments{
			Method: "Post",
			Router: `/`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:UserToolsController"] = append(beego.GlobalControllerRouter["bitbucket.org/fp-zhengyang.zhu/video-ads/api/controllers:UserToolsController"],
		beego.ControllerComments{
			Method: "Options",
			Router: `/*`,
			AllowHTTPMethods: []string{"options"},
			MethodParams: param.Make(),
			Params: nil})

}
